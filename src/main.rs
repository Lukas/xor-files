#[cfg(test)]
mod paramtest;

mod version;
mod worker;

use std::env::args;
use std::result::Result;
use std::process::exit;
use version::VERSION;
use worker::{Worker, XORWorker};

fn info(exe_name: &str) -> String {
    format!(concat!(
        "xor-files version {}\n\n",

        "Usage: {} inputfile-1 inputfile-2 outputfile\n\n",

        "Each file can be for example a regular file or a fifo pipe.\n",
        "The inputfiles are read, combined with xor and written to the ",
        "outputfile.\n\n",

        "If the first inputfile is larger, the second ",
        "is treated as if it was padded with\n",
        "nullbytes.\n",
        "If the second inputfile is larger, only ",
        "the size of the first inputfile is\n",
        "processed and the rest of the second file will be ignored."
    ), VERSION, exe_name)
}

fn perform<I, W>(mut arg_iter: I) -> Result<(), String>
        where I: Iterator,
        <I as Iterator>::Item: ToString,
        W: XORWorker {
    // Read parameters
    let exe_name: String = match arg_iter.next() {
        Some(x) => x.to_string(),
        None => "xor-files".to_string(),
    };
    let mut arg_vec: Vec<String> = Vec::with_capacity(3);
    for i in 0..3 {
        arg_vec.push(match arg_iter.next() {
            Some(x) => x.to_string(),
            None => return Err(
                format!(
                    "Expected 3 arguments, but got {}\n\n{}",
                    i, info(&exe_name)
                )
            ),
        });
    }
    // Search for more parameters
    let mut i = 3;
    loop {
        match arg_iter.next() {
            Some(_) => {
                i += 1;
            },
            None => break,
        };
    }
    if i > 3 {
        return Err(format!(
            "Expected 3 arguments, but got {}\n\n{}",
            i, info(&exe_name)
        ));
    }
    W::work([&arg_vec[0][..], &arg_vec[1][..]], &arg_vec[2][..])
}

fn main() {
    let exit_code = match perform::<_, Worker>(args()) {
        Ok(()) => 0,
        Err(e) => {
            eprintln!("{}", e);
            1
        }
    };
    exit(exit_code);
}
