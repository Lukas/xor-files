use crate::{info, perform};
use crate::worker::XORWorker;

// Test wrong number of arguments
#[test]
fn test_missing_exe_param() {
    let params: Vec<&'static str> = vec![];
    let result = perform::<_, TestWorker>(params.iter());
    assert_eq!(result, Err(
        format!(
            "Expected 3 arguments, but got 0\n\n{}",
            info("xor-files"),
        )
    ));
}

#[test]
fn test_zero_params() {
    let params = vec!["myxor"];
    let result = perform::<_, TestWorker>(params.iter());
    assert_eq!(result, Err(
        format!(
            "Expected 3 arguments, but got 0\n\n{}",
            info("myxor"),
        )
    ));
}

#[test]
fn test_one_param() {
    let params = vec!["myxor", "a"];
    let result = perform::<_, TestWorker>(params.iter());
    assert_eq!(result, Err(
        format!(
            "Expected 3 arguments, but got 1\n\n{}",
            info("myxor"),
        )
    ));
}

#[test]
fn test_two_params() {
    let params = vec!["myxor", "a", "b"];
    let result = perform::<_, TestWorker>(params.iter());
    assert_eq!(result, Err(
        format!(
            "Expected 3 arguments, but got 2\n\n{}",
            info("myxor"),
        )
    ));
}

#[test]
fn test_four_params() {
    let params = vec!["myxor", "a", "b", "c", "d"];
    let result = perform::<_, TestWorker>(params.iter());
    assert_eq!(result, Err(
        format!(
            "Expected 3 arguments, but got 4\n\n{}",
            info("myxor"),
        )
    ));
}

#[test]
fn test_five_params() {
    let params = vec!["myxor", "a", "b", "c", "d", "e"];
    let result = perform::<_, TestWorker>(params.iter());
    assert_eq!(result, Err(
        format!(
            "Expected 3 arguments, but got 5\n\n{}",
            info("myxor"),
        )
    ));
}

// Test right number of arguments
struct TestWorker;

impl XORWorker for TestWorker {
    fn work(input: [&str; 2], output: &str) -> Result<(), String> {
        Err(format!(
            "Operation:\n{} = {} xor {}",
            output, input[0], input[1]
        ))
    }
}

#[test]
fn test_three_params() {
    let params = vec!["myxor", "file-a", "file-b", "file-c"];
    let result = perform::<_, TestWorker>(params.iter());
    assert_eq!(result, Err(
        "Operation:\nfile-c = file-a xor file-b".to_string()
    ));
}
